package cz.hk.lundegaard.contactus.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author : vanya.melnykovych
 * @since : 19.01.2022
 */
@Data
@AllArgsConstructor
public class ContactUs {

    private Long requestTypeId;

    private String email;

    private Long policyNumber;

    private String name;

    private String surname;

    private String requestDescription;

}
