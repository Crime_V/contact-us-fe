package cz.hk.lundegaard.contactus.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author : vanya.melnykovych
 * @since : 19.01.2022
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RequestType {

    private Long id;

    private String name;
}
