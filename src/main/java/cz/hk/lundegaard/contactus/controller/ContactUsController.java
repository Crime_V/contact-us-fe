package cz.hk.lundegaard.contactus.controller;

import cz.hk.lundegaard.contactus.domain.ContactUs;
import cz.hk.lundegaard.contactus.domain.RequestType;
import org.apache.logging.log4j.util.Strings;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * @author : vanya.melnykovych
 * @since : 19.01.2022
 */
@Controller
public class ContactUsController {

    private final String BACK_END_REQUEST_TYPES_ENDPOINT = "http://localhost:8080/rest/api/request-types/all";
    private final String BACK_END_SAVE_CONTACT_US_ENDPOINT = "http://localhost:8080/rest/api/contact-us/save";

    private String successMessage = "";

    @GetMapping("/main")
    public String main(Model model) {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<RequestType[]> response = restTemplate.getForEntity(BACK_END_REQUEST_TYPES_ENDPOINT, RequestType[].class);
        List<RequestType> requestTypes = Arrays.asList(Objects.requireNonNull(response.getBody()));
        model.addAttribute("requestTypes", requestTypes);
        if (!Strings.isEmpty(successMessage)) {
             model.addAttribute("successMessage", successMessage);
        }
        return "contact-us";
    }

    @PostMapping("/main")
    public String sendContactUsRequest(@RequestParam Long requestTypeId,
                                       @RequestParam String email,
                                       @RequestParam Long policyNumber,
                                       @RequestParam String name,
                                       @RequestParam String surname,
                                       @RequestParam String requestDescription) {
        ContactUs contactUs = new ContactUs(requestTypeId, email, policyNumber, name, surname, requestDescription);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Object> response;
        response = restTemplate.postForEntity(BACK_END_SAVE_CONTACT_US_ENDPOINT, contactUs, Object.class);
        if (response.getStatusCodeValue() == 200) {
            Object o = Objects.requireNonNull(response.getBody());
            successMessage = Objects.requireNonNull(response.getBody()).toString();
        }
        return "redirect:/main";
    }
}
